#!/usr/bin/fish
if cat /proc/version | grep fedora
	sudo dnf install git
	sudo dnf install cmus
	sudo dnf install libcurl-devel
	sudo dnf install openssl-devel
end
builtin cd ~/bin/
git clone https://github.com/Arkq/cmusfm
builtin cd cmusfm/
autoreconf --install
mkdir build
builtin cd build/
../configure
make
sudo make install
cmusfm init
cd ..
rm build
#cmus feh art
builtin cd ~/.config/cmus
git clone https://github.com/TiredSounds/cmus-scripts
builtin cd cmus-scripts/
sed -i '32s,.*,		setsid feh -g 900x900+1160+546 -x --zoom fill "$ART" &,' cmus-feh.sh
