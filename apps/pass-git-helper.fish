#!/usr/bin/fish
if cat /proc/version | grep fedora
	sudo dnf install git
	sudo dnf install pass
	sudo dnf isntall python3
end

builtin cd ~/bin/
git clone https://github.com/languitar/pass-git-helper.git
cd ~/bin/pass-git-helper
python3 setup.py install --user
builtin cd ~/.config/
mkdir pass-git-helper
cd pass-git-helper/
git config --global credential.helper /home/user/bin/pass-git-helper/pass-git-helper
echo "[github.com*]
target=development/github/keys/schvabodka-man
skip_username=0" > git-pass-mapping.ini
echo "

[*bitbucket.com]
target=development/atlassian/scvhapps@gmail.com
skip_username=0" >> git-pass-mapping.ini
