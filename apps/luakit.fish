#!/usr/bin/fish
if cat /proc/version | grep fedora
	sudo dnf install youtube-dl
	#sudo dnf install mpv
	sudo dnf install git
	sudo dnf install compat-lua
	sudo dnf install compat-lua-devel
	sudo dnf install sqlite
	sudo dnf install sqlite-devel
	sudo dnf install gtk3
	sudo dnf install gtk3-devel
	sudo dnf install webkitgtk
	sudo dnf install webkitgtk3
	sudo dnf install webkitgtk4
	sudo dnf install webkitgtk-devel
	sudo dnf install webkitgtk3-devel
	sudo dnf install webkitgtk4-devel
	sudo dnf install lua-filesystem-compat
end
builtin cd ~/bin/
git clone git://github.com/luakit/luakit
builtin cd luakit
make
sudo make install
make clean

builtin cd ~/.local/share/luakit/scripts

# wget https://bitbucket.org/b0r3d0m/lorify-userscript/raw/a26f66615cee33384eb7394ba3c5b0a33341374e/lorify.user.js
wget https://www.4chan-x.net/builds/4chan-X.user.js
wget https://userstyles.org/styles/userjs/146381/4chan-minimalistic-dark-theme.user.js
wget https://userstyles.org/styles/userjs/113036/dark-gog-com.user.js
# wget https://userstyles.org/styles/userjs/132238/dark-and-cleaner-imdb.user.js
# wget https://userstyles.org/styles/userjs/136318/clear-dark-facebook-by-book777.user.js
# wget https://userstyles.org/styles/userjs/135366/twitter-dark-mode-2017.user.js
# wget https://userstyles.org/styles/userjs/85419/twitchtv-lights-out-new.user.js
# wget https://userstyles.org/styles/userjs/117574/2ch-simple-dark.user.js
# wget https://userstyles.org/styles/userjs/134785/2ch-reset.user.js
# wget https://userstyles.org/styles/userjs/141275/blacktube.user.js
# wget https://userstyles.org/styles/userjs/118959/darksearch-for-google.user.js
# wget https://userstyles.org/styles/userjs/120329/ebay-dark-black-midnight-smacked.user.js
# wget https://userstyles.org/styles/userjs/107360/soundcloud-com-nightmode.user.js
# wget https://userstyles.org/styles/userjs/37035/github-dark.user.js
# wget https://userstyles.org/styles/userjs/35345/stackoverflow-dark.user.js
# wget https://userstyles.org/styles/userjs/122072/wikipedia-minimalistic-dark-material-design.user.js

git clone https://github.com/luakit/luakit-plugins.git ~/.config/luakit/plugins
