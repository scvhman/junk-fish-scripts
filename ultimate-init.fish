#!/usr/bin/fish
mkdir ~/bin
if cat /proc/version | grep fedora
	sudo dnf install pass
	sudo dnf install git
	sudo dnf install feh
	sudo dnf install shntool
	sudo dnf install cuetools
	sudo dnf install p7zip
	sudo dnf install docker
	sudo dnf install unrar
	sudo dnf install powerline-fonts
	sudo dnf install thefuck
	sudo dnf install autojump-fish
	sudo dnf install avfs
	
	sudo dnf isntall python3
	sudo dnf isntall python
	sudo dnf isntall rubygems
	sudo dnf install flatpak

	sudo dnf install firefox
	sudo dnf install chromium
	sudo dnf install emacs
	sudo dnf install calibre

	sudo dnf install beets
	sudo dnf install beets-plugins
	sudo dnf install progress

	sudo dnf install python-devel
	sudo dnf install ruby-devel
	sudo dnf install gcc-c++
	sudo dnf install autoconf automake
	sudo dnf install cmake
	sudo dnf install scala
	sudo dnf install go
	sudo dnf install brainfuck
	sudo dnf install java-1.8.0-openjdk-src
	sudo dnf install cpan
	
	sudo dnf install libcurl-devel
	sudo dnf install openssl-devel
	
	sudo dnf install luarocks
	sudo dnf install lua-devel
	
	sudo dnf install pandoc
	sudo dnf install tig

	sudo dnf install libpng12
	sudo dnf install libpng15

	sudo dnf install NetworkManager-tui
	sudo dnf install htop

	sudo dnf install hunspell
	sudo dnf install hunspell-ru

	sudo dnf install ShellCheck #emacs shell scripts linting

	sudo dnf install tidy
	sudo dnf install csslint
	
	#вменяемая раскладка в имаксе
	sudo dnf install scim scim-tables scim-m17n
end

#fish shell 
curl -L http://get.oh-my.fish | fish # oh-my-fish
omf install bobthefish
omf install sudope
omf install thefuck
omf install z

go get -u github.com/gpmgo/gopm
builtin cd /home/user/Go/bin/
./gopm bin github.com/jingweno/ccat
./gopm bin github.com/nishanths/license

#fzf
builtin cd ~/bin
git clone https://github.com/junegunn/fzf.git
builtin cd fzf
./install
mkdir ~/.config/fzf
builtin ~/.config/fzf
touch history
# path picker
builtin cd ~/bin
git clone https://github.com/facebook/PathPicker.git

sudo pip install mycli
sudo pip install pgcli
sudo gem install meetup-cli
sudo npm install -g @aweary/alder
sudo npm install cloc -g

sudo luarocks install --server=http://luarocks.org/dev luash #for the shell scripting

sudo npm install -g tern
sudo npm -g install js-beautify
sudo npm -g install jsonlint

# sudo cpan i IPC::Run3
# sudo cpan i Log::Log4perl
# sudo cpan i CSS::Watcher

#googler
builtin cd ~/bin/
git clone https://github.com/jarun/googler/

#ls with icons
cd ~/bin
wget https://raw.githubusercontent.com/AnthonyDiGirolamo/els/master/els
chmod +x els

#fish bookmarks
git clone http://github.com/techwizrd/fishmarks.git ~/.fishmarks

#flatpaks
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flatflatpak --user remote-add gnome-nightly gnome-nightly.flatpakrepohub.flatpakrepo
flatpak install flathub org.musicbrainz.Picard
flatpak install flathub com.valvesoftware.Steam
wget https://sdk.gnome.org/keys/gnome-sdk.gpg
flatpak remote-add --gpg-import=gnome-sdk.gpg gnome https://sdk.gnome.org/repo/
flatpak install gnome org.gnome.Platform $FLATPACK_GNOME
flatpak install gnome org.gnome.Platform.Locale $FLATPACK_GNOME
flatpak install gnome org.gnome.Sdk $FLATPACK_GNOME
#twitch
flatpak install --from https://dl.tingping.se/flatpak/gnome-twitch.flatpakref
#skype
flatpak install --from https://s3.amazonaws.com/alexlarsson/skype-repo/skype.flatpakref
#emulators for meme games
flatpak install flathub org.ppsspp.PPSSPP
flatpak install flathub org.libretro.RetroArch
#meme gnome apps
flatpak remote-add --gpg-import=nightly.gpg gnome-nightly-apps https://sdk.gnome.org/nightly/repo-apps/
#gnu cash
flatpak install flathub org.gnucash.GnuCash

#powerline fonts
builtin cd ~/bin/
git clone https://github.com/powerline/fonts.git --depth=1
cd fonts
./install.sh
