#!/usr/bin/fish
sudo dnf install mame #MAME
sudo dnf install gens-gs #Sega Genesis
sudo dnf install fceux #NES
sudo dnf install pcsxr #PS1
sudo dnf install stella #Atari 2600
sudo dnf install snes9x #Snes
sudo dnf install snes9x-gtk #Snes9x frontend
sudo dnf install meka #Sega master system
sudo dnf install lxdream #Sega dreamcast
